# HN FEED

This web application displays the latest posts from hackernews that match the search query of nodejs, you can delete posts from your feed and just forget about seeing them again. Posts are synced once the api server starts and once every hour.

The client is built with create-react-app and served through nginx in production. The backend consists of a NestJS API, accessible under the /api/ endpoint through a reverse proxy. 

## Requirements

Both client and server are containerized so you'll need docker. For development, you can easily start the project within VSCode with the VSCode Containers extension.

## PROJECT SETUP

Clone this repository

>`git@gitlab.com:_hyphen/hnfeed.git`

Next run:

```sh
docker-compose -p hnfeed up
```

Visit http://localhost

## DEVELOPMENT

Open a shell on the vscode container, then run:

```sh
yarn start
```

Visit http://localhost:8000

The api is also accessible at http://localhost:4000

----

If you don't use vscode, follow the instructions below to start the development environment:

Run:

```sh
docker-compose -p hnfeed_dev -f docker-compose.yml -f .devcontainer/docker-compose.yml up -d
```

Attach a shell to the running container

```sh
docker exec -it hnfeed_dev_vscode_1 zsh
```

The current directory will be mounted to /app so we need to change our directory there

```sh
cd /app
```
