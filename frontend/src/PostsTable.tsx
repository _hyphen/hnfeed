import axios from "axios";
import { differenceInHours, format, parseISO } from "date-fns";
import * as React from "react";
import styles from "./PostsTable.module.scss";
import TrashIcon from "./TrashIcon";

export type Post = {
  [k: string]: any;
  _id: string;
  story_title: string | null;
  title: string | null;
  story_url: string | null;
  url: string | null;
  author: string;
  created_at: string;
};

function getFormatedDate(date: Date): string {
  const diff = differenceInHours(new Date(), date);
  switch (true) {
    case diff < 24:
      return format(date, "h:mm aaa");
    case diff < 48:
      return "Yesterday";
    default:
      return format(date, "MMM d");
  }
}

const RowState = {
  loading: "loading" as "loading",
  ready: "ready" as "ready",
  deleted: "deleted" as "deleted",
};
type RowState = typeof RowState[keyof typeof RowState];

const TableRow = ({ _id, title, story_url, url, author, created_at }: Post) => {
  const link_url = (story_url || url) as string;
  const [rowState, setRowState] = React.useState<RowState>("ready");
  const handleRowClick = () => {
    window.open(link_url, "_blank");
  };

  const deleteItem = async (e: React.MouseEvent) => {
    e.stopPropagation();
    if (rowState === RowState.loading) return;
    setRowState(RowState.loading);
    try {
      await axios.delete(`/api/posts/${_id}`);
      setRowState("deleted");
    } catch (error) {
      setRowState("ready");
    }
  };

  if (rowState === "deleted") return null;

  return (
    <div id={_id} onClick={handleRowClick} className={styles.row}>
      <div className={styles.data}>
        <div>
          <span className={styles.story_title}>{title}</span>
          <span className={styles.author}> - {author} -</span>
        </div>
        <span>{getFormatedDate(parseISO(created_at))}</span>
      </div>
      <button
        className={styles.deleteBtn}
        disabled={rowState === RowState.loading}
        onClick={deleteItem}
      >
        <TrashIcon />
      </button>
    </div>
  );
};

type TableProps = {
  items: Post[];
};

const PostsTable = ({ items }: TableProps) => {
  return (
    <div className={styles.table}>
      {items
        .filter(({ story_title, title }) => story_title || title)
        .map(({ title, ...post }) => {
          const postTitle = (title || post.story_title) as string;
          return <TableRow key={post._id} title={postTitle} {...post} />;
        })}
    </div>
  );
};

export default PostsTable;
