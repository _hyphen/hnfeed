import React from "react";
import { render, screen } from "@testing-library/react";
import PostsTable from "./PostsTable";
import { subHours } from "date-fns";
import userEvent from "@testing-library/user-event";

const makePost = () => {
  const PostWithData = {
    _id: (Math.random() * 1e8).toString(16),
    story_title: "title",
    title: null,
    story_url: "http://localhost",
    url: null,
    author: "Tolkien",
    created_at: "2021-02-20T05:20:43.000Z",
  };
  return PostWithData;
};

const OtherPost = {
  _id: "123456",
  story_title: null,
  title: "this one has no story title",
  story_url: "http://localhost",
  url: null,
  author: "Tolkien",
  created_at: "2021-02-20T05:20:43.000Z",
};

describe("Posts table", () => {
  beforeAll(() => {
    jest.useFakeTimers("modern");
    jest.setSystemTime(new Date("2021-02-20T05:20:43.000Z"));
  });
  test("should display records", async () => {
    render(<PostsTable items={[makePost(), OtherPost]} />);

    expect(await screen.findByText(makePost().story_title)).toBeInTheDocument();
    expect(await screen.findByText(OtherPost.title)).toBeInTheDocument();
  });

  test("should format the post date based on how much time has passed", async () => {
    const today = new Date("2021-02-20T00:20:43.000Z");
    const yesterday = subHours(today, 24);
    const pastYesterday = subHours(today, 49);
    const newPost = { ...makePost(), created_at: today.toJSON() };
    const oldPost = { ...makePost(), created_at: yesterday.toJSON() };
    const olderPost = { ...makePost(), created_at: pastYesterday.toJSON() };
    render(<PostsTable items={[newPost, oldPost, olderPost]} />);

    expect(await screen.findByText("12:20 am")).toBeInTheDocument();
    expect(await screen.findByText("Yesterday")).toBeInTheDocument();
    expect(await screen.findByText("Feb 17")).toBeInTheDocument();
  });

  test("should redirect to post url after clicking the row", async () => {
    const mock = jest.fn();
    jest.spyOn(window, "open").mockImplementation(mock);
    const post = makePost();
    render(<PostsTable items={[post]} />);

    expect(await screen.findByText("5:20 am")).toBeInTheDocument();
    const row = document.getElementById(post._id) as HTMLElement;
    userEvent.click(row);
    expect(mock).toHaveBeenCalled();
  });
});
