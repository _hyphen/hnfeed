import * as React from "react";
import styles from "./Layout.module.scss";

const Header = () => (
  <header className={styles.header}>
    <h1 className={styles.title}>HN Feed</h1>
    <span className={styles.text}>We {"<3"} hacker news!</span>
  </header>
);

const Layout: React.FC = ({ children }) => {
  return (
    <React.Fragment>
      <Header />
      {children}
    </React.Fragment>
  );
};

export default Layout;
