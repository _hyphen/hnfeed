import axios from "axios";
import * as React from "react";
import Layout from "./Layout";
import PostsTable, { Post } from "./PostsTable";

function App() {
  const [items, setItems] = React.useState<Post[]>([]);

  React.useEffect(() => {
    async function loadPosts(): Promise<Post[]> {
      let data: Post[] = [];
      try {
        ({ data } = await axios.get<Post[]>("/api/posts"));
      } catch (err) {
        return [];
      }
      return data;
    }

    loadPosts().then(setItems);
  }, []);
  return (
    <Layout>
      <PostsTable items={items} />
    </Layout>
  );
}

export default App;
