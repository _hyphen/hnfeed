import { HttpModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { SyncFeedModule } from './sync-feed/sync-feed.module';
import { PostModule } from './post/post.module';

@Module({
  imports: [
    MongooseModule.forRoot(global.process.env['MONGO_URL']),
    ScheduleModule.forRoot(),
    SyncFeedModule,
    PostModule,
    HttpModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
