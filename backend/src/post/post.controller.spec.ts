import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { Post, PostSchema } from './schemas/post.schema';

describe('PostController', () => {
  let controller: PostController;

  beforeEach(async () => {
    function mockPostModel(dto: any) {
      this.data = dto;
      this.save = () => {
        return this.data;
      };
    }

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [PostController],
      providers: [
        PostService,
        {
          provide: getModelToken('Post'),
          useValue: mockPostModel,
        },
      ],
    }).compile();

    controller = module.get<PostController>(PostController);
  });

  it('should be defined', () => {});
});
