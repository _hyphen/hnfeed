import * as mongoose from 'mongoose';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop()
  created_at: Date;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  deleted_at: Date | null;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  story_url: string | null;

  @Prop()
  author: string;

  @Prop()
  points: number;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  story_title: string | null;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  story_text: string | null;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  comment_text: string | null;

  @Prop([String])
  _tags: string[];

  @Prop()
  num_comments: number;

  @Prop()
  objectID: string;

  @Prop(
    raw({
      title: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        matchedWords: {
          type: [String],
        },
      },
      url: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        matchedWords: {
          type: [String],
        },
      },
      author: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        matchedWords: {
          type: [String],
        },
      },
    }),
  )
  _highlightResult: Record<string, any>;
}

export const PostSchema = SchemaFactory.createForClass(Post);
