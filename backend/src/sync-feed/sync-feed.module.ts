import { HttpModule, Module, OnApplicationBootstrap } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Post, PostSchema } from 'src/post/schemas/post.schema';
import { SyncFeedService } from './sync-feed.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    HttpModule,
  ],
  providers: [SyncFeedService],
})
export class SyncFeedModule implements OnApplicationBootstrap {
  constructor(private readonly syncService: SyncFeedService) {}
  onApplicationBootstrap() {
    this.syncService.handleCron();
  }
}
