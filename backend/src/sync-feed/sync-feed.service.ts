import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AxiosResponse } from 'axios';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Post, PostDocument } from '../post/schemas/post.schema';

const feed = 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';

@Injectable()
export class SyncFeedService {
  private readonly logger = new Logger(SyncFeedService.name);
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    private readonly httpService: HttpService,
  ) {}

  @Cron('0 * * * *', { name: 'syncPostsWithHN' })
  async handleCron() {
    this.logger.log('Syncing posts with hackernews api....');
    let response: AxiosResponse;

    try {
      response = await this.httpService.get(feed).toPromise();
    } catch (error) {
      console.log('Could not connect to hackernews api');
      return;
    }

    if (response?.data) {
      const data = response.data.hits as Post[];

      try {
        const opResult = await this.postModel.bulkWrite(
          data.map((document) => {
            return {
              updateOne: {
                filter: { objectID: { $eq: document.objectID } },
                update: document,
                upsert: true,
              },
            };
          }),
        );

        const { upsertedCount, modifiedCount } = opResult;

        this.logger.log('Posts synced to db!');
        this.logger.log(
          `New Posts: ${upsertedCount}, Updated: ${modifiedCount}`,
        );
      } catch (error) {
        this.logger.error('Could not update posts');
      }
    }
  }
}
