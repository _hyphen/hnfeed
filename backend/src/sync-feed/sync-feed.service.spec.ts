import { HttpService } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { SyncFeedService } from './sync-feed.service';

describe('SyncFeedService', () => {
  let service: SyncFeedService;

  beforeEach(async () => {
    function mockPostModel(dto: any) {
      this.data = dto;
      this.save = () => {
        return this.data;
      };
    }
    function mockHttpService(dto: any) {
      this.data = dto;
      this.get = () => {
        return this.data;
      };
    }
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SyncFeedService,
        {
          provide: getModelToken('Post'),
          useValue: mockPostModel,
        },
        {
          provide: HttpService,
          useValue: mockHttpService,
        },
      ],
    }).compile();

    service = module.get<SyncFeedService>(SyncFeedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
